# RRW (Rust REST Wrapper)

A crate to easily build clients for REST-APIs.

## Example

```rust
#[derive(Serialize)]
struct LocationQuery {
    query: String,
}

#[derive(Deserialize, Debug, PartialEq, Eq)]
struct Location {
    id: String,
    name: String,
}

#[rest]
impl Bahn {
    async fn location(&self, location: &LocationQuery) -> Result<Vec<Location>, reqwest::Error> {
        RestRequest::<&LocationQuery, ()>::get("/locations").query(location)
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let bahn = Bahn::new(RestConfig::new("https://v5.db.transport.rest"));
    let berlin = LocationQuery {
        query: "Berlin".to_string(),
    };

    let results = bahn.location(&berlin).await?;

    for location in results {
        println!("{}: {}", location.id, location.name);
    }

    Ok(())
}
```

## Features

- Easily build a REST-Wrapper
- Included throttle mechanism (currently not very advanced, but expandable).

## Future Features

- More advanced throttle mechanisms.
- Authentication (Username/Password, OAuth).
