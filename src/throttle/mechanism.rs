use std::time::{Duration, Instant};

/// A mechanism that specifies how much a request should be throttled.
///
/// Note that using such a mechanism might not guarantee that there will never by slow-down
/// requests, as for example the clocks on the client and server may run differently.
///
/// # Example
///
/// A very dump example specifying that every request should wait one second:
///
///
/// ```rust
/// # use std::error::Error;
/// # use std::time::{Duration, Instant};
/// # use rrw::throttle::ThrottleMechanism;
/// struct OneSecondThrottle {}
///
/// impl ThrottleMechanism for OneSecondThrottle {
///     fn timeout_delay(&mut self, _now: Instant) -> Duration {
///         Duration::from_secs(1)
///     }
/// }
///
/// #
/// # fn main() -> Result<(), Box<dyn Error>> {
/// #     Ok(())
/// # }
/// ```

pub trait ThrottleMechanism {
    /// Given a [Instant] when the request wants to take place, calculate the duration of how long
    /// the request should wait.
    ///
    /// This method may (and in most cases must) change its internal states, for example to store
    /// the number of already submitted requests.
    ///
    /// The [Instant]-parameter will only increase as time goes on, it will never decrease.
    fn timeout_delay(&mut self, now: Instant) -> Duration;
}
