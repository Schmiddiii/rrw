//! Utilities for throttling requests.

mod mechanism;
mod stupid;
mod throttler;

pub use mechanism::ThrottleMechanism;
pub use stupid::StupidThrottle;
pub use throttler::Throttle;
