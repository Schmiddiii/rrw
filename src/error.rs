use std::fmt::{Debug, Display};

use serde::Deserialize;

/// The errors that may occure when accessing a REST-API.
///
/// To determine the type of error, the following things are done in order:
///
/// - Send a HTTP(S)-Request. If failed, return [Error::Request].
/// - Try to parse it into the wanted return type. If failed:
/// - Try to parse it into the error type `T`. If successfull, return [Error::Api]
/// - Otherwise return a [Error::BodyNotParsable] with the error from trying to convert into the
/// wanted return type.
///
/// One common type for `T` is [StandardRestError], but `T` can be custom as log as
/// `T: Display + Debug` is satisfied.
#[derive(Debug)]
pub enum Error<T> {
    /// Failed to send the request or parse the bytes of the response.
    Request(reqwest::Error),
    /// The API returned a error.
    Api(T),
    /// The API returned something unreadable.
    BodyNotParsable(serde_json::Error),
}

impl<T: Display> Display for Error<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Request(e) => write!(f, "Failed to request: {}", e),
            Error::Api(e) => write!(f, "Api gave an error: {}", e),
            Error::BodyNotParsable(e) => write!(f, "The response body could not be parsed {}", e),
        }
    }
}

impl<T: Display + Debug> std::error::Error for Error<T> {}

/// A standard error scheme found in many REST-API responses.
///
/// This consists of a status code, a error indicator and a message.
///
/// A example of a [StandardRestError] in JSON would be:
///
/// ```json
/// {
///   "statusCode": 400,
///   "error": true,
///   "msg": "Missing query parameter."
/// }
/// ```
///
/// Should be used in combination with [Error].
///
#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct StandardRestError {
    /// The error code return from the API.
    pub status_code: usize,
    /// A indicator that a error occured.
    pub error: bool,
    /// A error message.
    pub msg: String,
}

impl Display for StandardRestError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}
