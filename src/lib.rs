//! # RRW (Rust REST Wrapper)
//!
//! A crate to easily build clients for REST-APIs.
//!
//! There is a auxiliary crate, `rrw_macro` available that simplifies the generation of REST-APIs
//! even more.
//!
//! # Example
//!
//! Structs for request parameters and result types:
//!
//! ```rust
//! # use rrw::{RestConfig, RestRequest};
//! # use serde::{Deserialize, Serialize};
//! #
//! #[derive(Serialize)]
//! struct LocationQuery {
//!     query: String,
//! }
//!
//! #[derive(Deserialize, Debug, PartialEq, Eq)]
//! struct Location {
//!     id: String,
//!     name: String,
//! }
//! ```
//!
//! The REST-Wrapper without macros.
//!
//! ```rust
//! # use rrw::{RestConfig, RestRequest, Error, StandardRestError};
//! # use serde::{Deserialize, Serialize};
//! #
//! # #[derive(Serialize)]
//! # struct LocationQuery {
//! #     query: String,
//! # }
//! #
//! # #[derive(Deserialize, Debug, PartialEq, Eq)]
//! # struct Location {
//! #     id: String,
//! #     name: String,
//! # }
//! #
//! struct Bahn {
//!     rest: RestConfig,
//! }
//!
//! impl Bahn {
//!     pub fn new(rest: RestConfig) -> Self {
//!         Self { rest }
//!     }
//! }
//!
//! impl Bahn {
//!     async fn location(
//!         &self, location: &LocationQuery
//!     ) -> Result<Vec<Location>, Error<StandardRestError>> {
//!         Ok(self
//!             .rest
//!             .execute(&RestRequest::<&LocationQuery, ()>::get("/locations").query(location))
//!             .await?)
//!     }
//! }
//! ```
//!
//! The REST-Wrapper with macros.
//!
//! ```rust
//! # extern crate r#rrw;
//! # use rrw::{RestConfig, RestRequest, Error, StandardRestError};
//! # use serde::{Deserialize, Serialize};
//! # use rrw_macro::rest;
//! #
//! # #[derive(Serialize)]
//! # struct LocationQuery {
//! #     query: String,
//! # }
//! #
//! # #[derive(Deserialize, Debug, PartialEq, Eq)]
//! # struct Location {
//! #     id: String,
//! #     name: String,
//! # }
//! #
//! #[rest]
//! impl Bahn {
//!     async fn location(
//!         &self, location: &LocationQuery
//!     ) -> Result<Vec<Location>, Error<StandardRestError>> {
//!         RestRequest::<&LocationQuery, ()>::get("/locations").query(location)
//!     }
//! }
//!
//! ```
//!
//! Usage:
//!
//! ```rust
//! # extern crate r#rrw;
//! # use rrw::{RestConfig, RestRequest, Error, StandardRestError};
//! # use serde::{Deserialize, Serialize};
//! #
//! # #[derive(Serialize)]
//! # struct LocationQuery {
//! #     query: String,
//! # }
//! #
//! # #[derive(Deserialize, Debug, PartialEq, Eq)]
//! # struct Location {
//! #     id: String,
//! #     name: String,
//! # }
//! #
//! # struct Bahn {
//! #     rest: RestConfig,
//! # }
//! #
//! # impl Bahn {
//! #     pub fn new(rest: RestConfig) -> Self {
//! #         Self { rest }
//! #     }
//! # }
//! #
//! # impl Bahn {
//! #     async fn location(&self, location: &LocationQuery) -> Result<Vec<Location>, Error<StandardRestError>> {
//! #         Ok(self
//! #             .rest
//! #             .execute(&RestRequest::<&LocationQuery, ()>::get("/locations").query(location))
//! #             .await?)
//! #     }
//! # }
//! #
//! # #[tokio::main]
//! # async fn main() -> Result<(), Error<StandardRestError>> {
//! #    env_logger::init();
//! let bahn = Bahn::new(RestConfig::new("https://v5.db.transport.rest"));
//! let berlin = LocationQuery {
//!     query: "Berlin".to_string(),
//! };
//!
//! let results = bahn.location(&berlin).await?;
//!
//! for location in results {
//!     println!("{}: {}", location.id, location.name);
//! }
//! #    Ok(())
//! # }
//! ```
//!
//! More examples can be found in the examples directory.

pub mod authenticate;
mod config;
mod error;
mod request;
pub mod throttle;

pub use config::{RestConfig, RestConfigBuilder};
pub use error::{Error, StandardRestError};
pub use request::RestRequest;
