use rrw::{Error, RestConfig, RestRequest, StandardRestError};
use rrw_macro::rest;
use serde::{Deserialize, Serialize};

#[derive(Serialize)]
struct LocationQuery {
    query: String,
}

#[derive(Deserialize, Debug, PartialEq, Eq)]
struct Location {
    id: String,
    name: String,
}

#[rest]
impl Bahn {
    async fn location(
        &self,
        location: &LocationQuery,
    ) -> Result<Vec<Location>, Error<StandardRestError>> {
        RestRequest::<&LocationQuery, ()>::get("/locations").query(location)
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let bahn = Bahn::new(RestConfig::new("https://v5.db.transport.rest"));
    let berlin = LocationQuery {
        query: "Berlin".to_string(),
    };

    let results = bahn.location(&berlin).await?;

    for location in results {
        println!("{}: {}", location.id, location.name);
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn bahn() -> Result<(), Box<dyn std::error::Error>> {
        let bahn = Bahn::new(RestConfig::new("https://v5.db.transport.rest"));
        let berlin = LocationQuery {
            query: "Berlin".to_string(),
        };

        let results = bahn.location(&berlin).await?;

        assert!(results.contains(&Location {
            id: "8096003".to_string(),
            name: "BERLIN".to_string()
        }));

        Ok(())
    }
}
