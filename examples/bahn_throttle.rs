use rrw::throttle::StupidThrottle;
use rrw::{Error, RestConfigBuilder, RestRequest, StandardRestError};
use rrw_macro::rest;
use serde::{Deserialize, Serialize};

#[derive(Serialize)]
struct LocationQuery {
    query: String,
}

#[derive(Deserialize, Debug, PartialEq, Eq)]
struct Location {
    id: String,
    name: String,
}

#[rest]
impl Bahn {
    async fn location(
        &self,
        location: &LocationQuery,
    ) -> Result<Vec<Location>, Error<StandardRestError>> {
        RestRequest::<&LocationQuery, ()>::get("/locations").query(location)
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    // Allow only one request every 10 seconds.
    let config = RestConfigBuilder::new("https://v5.db.transport.rest")
        .throttle(StupidThrottle::new(6.0))
        .build();
    let bahn = Bahn::new(config);
    let berlin = LocationQuery {
        query: "Berlin".to_string(),
    };
    let munich = LocationQuery {
        query: "München".to_string(),
    };

    let results_berlin = bahn.location(&berlin).await?;

    for location in results_berlin {
        println!("{}: {}", location.id, location.name);
    }

    let results_munich = bahn.location(&munich).await?;

    for location in results_munich {
        println!("{}: {}", location.id, location.name);
    }

    Ok(())
}
