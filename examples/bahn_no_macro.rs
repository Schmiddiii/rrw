use rrw::{Error, RestConfig, RestRequest, StandardRestError};
use serde::{Deserialize, Serialize};

#[derive(Serialize)]
struct LocationQuery {
    query: String,
}

#[derive(Deserialize, Debug, PartialEq, Eq)]
struct Location {
    id: String,
    name: String,
}

struct Bahn {
    rest: RestConfig,
}

impl Bahn {
    pub fn new(rest: RestConfig) -> Self {
        Self { rest }
    }
}

impl Bahn {
    async fn location(
        &self,
        location: &LocationQuery,
    ) -> Result<Vec<Location>, Error<StandardRestError>> {
        Ok(self
            .rest
            .execute(&RestRequest::<&LocationQuery, ()>::get("/locations").query(location))
            .await?)
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let bahn = Bahn::new(RestConfig::new("https://v5.db.transport.rest"));
    let berlin = LocationQuery {
        query: "Berlin".to_string(),
    };

    let results = bahn.location(&berlin).await?;

    for location in results {
        println!("{}: {}", location.id, location.name);
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn bahn_no_macro() -> Result<(), Box<dyn std::error::Error>> {
        let bahn = Bahn::new(RestConfig::new("https://v5.db.transport.rest"));
        let berlin = LocationQuery {
            query: "Berlin".to_string(),
        };

        let results = bahn.location(&berlin).await?;

        assert!(results.contains(&Location {
            id: "8096003".to_string(),
            name: "BERLIN".to_string()
        }));

        Ok(())
    }
}
