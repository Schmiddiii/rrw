use rrw::{
    authenticate::HeaderAuthenticator, Error, RestConfigBuilder, RestRequest, StandardRestError,
};
use rrw_macro::rest;
use serde::{Deserialize, Serialize};

#[derive(Serialize)]
struct SearchQuery {
    scope: String,
    search: String,
}

#[derive(Deserialize, Debug, PartialEq, Eq)]
struct Project {
    name: String,
    description: String,
}

#[rest]
impl GitLab {
    async fn search(&self, search: &SearchQuery) -> Result<Vec<Project>, Error<StandardRestError>> {
        RestRequest::<&SearchQuery, ()>::get("/search")
            .query(search)
            .authenticate()
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let authenticator = HeaderAuthenticator::new(
        "PRIVATE-TOKEN",
        std::env::var("GITLAB_TOKEN").expect("Environmental variable `GITLAB_TOKEN` needed."),
    );
    let gitlab = GitLab::new(
        RestConfigBuilder::new("https://gitlab.com/api/v4")
            .authenticator(authenticator)
            .build(),
    );
    let query = SearchQuery {
        scope: "projects".to_string(),
        search: "gitlab".to_string(),
    };

    let results = gitlab.search(&query).await?;

    for project in results {
        println!("{}: {}", project.name, project.description);
    }

    Ok(())
}
