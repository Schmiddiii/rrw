# RRW-Macro (Rust REST Wrapper)

A macro crate for rrw to easily build clients for REST-APIs.

## Example

```rust
#[rest]
impl Bahn {
    async fn location(&self, location: &LocationQuery) -> Result<Vec<Location>, reqwest::Error> {
        RestRequest::<&LocationQuery, ()>::get("/locations").query(location)
    }
}
```

For more information, look at the rrw-crate.
