//! # RRW-Macro (Rust REST Wrapper)
//!
//! A crate bringing a easy-to-use macro for usage with the crate `rrw` to build REST-clients.
//!
//! # Example
//!
//! ```rust
//! # use rrw::{RestConfig, RestRequest, Error, StandardRestError};
//! # use serde::{Deserialize, Serialize};
//! # use rrw_macro::rest;
//! #
//! # #[derive(Serialize)]
//! # struct LocationQuery {
//! #     query: String,
//! # }
//! #
//! # #[derive(Deserialize, Debug, PartialEq, Eq)]
//! # struct Location {
//! #     id: String,
//! #     name: String,
//! # }
//! #
//! #[rest]
//! impl Bahn {
//!     async fn location(
//!         &self, location: &LocationQuery
//!     ) -> Result<Vec<Location>, Error<StandardRestError>> {
//!         RestRequest::<&LocationQuery, ()>::get("/locations").query(location)
//!     }
//! }
//! ```
//!
//! Further examples can be found in the `rrw` crate.

extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, parse_quote, ImplItem, ItemImpl};

fn map_method(method: &ImplItem) -> ImplItem {
    match method {
        ImplItem::Method(method) => {
            let mut modified = method.clone();
            let block = &method.block;
            modified.block = parse_quote!({
                let request = #block;
                Ok(self._rest.execute(&request).await?)
            });
            ImplItem::Method(modified)
        }
        _ => method.clone(),
    }
}

/// A macro to simply programm a REST-wrapper.
///
/// # Example
///
/// This example also includes a explanation on what is happening.
///
/// ```rust
/// # extern crate r#rrw;
/// # use rrw::{RestConfig, RestRequest, Error, StandardRestError};
/// # use serde::{Deserialize, Serialize};
/// # use rrw_macro::rest;
/// #
/// # #[derive(Serialize)]
/// # struct LocationQuery {
/// #     query: String,
/// # }
/// #
/// # #[derive(Deserialize, Debug, PartialEq, Eq)]
/// # struct Location {
/// #     id: String,
/// #     name: String,
/// # }
/// #
/// // The macro will automatically generate the needed `struct` including internal data. Do not
/// // explicitly write the struct.
/// #[rest]
/// impl Bahn {
///     // Every function will have this format:
///     // - `async`.
///     // - Name can be freely chosen.
///     // - Arguments `&self` and any other things needed to build the request.
///     // - Return a `Result` with either the resulting type you want or `rrw::Error` the type of
///     // the error can be freely customized.
///     async fn location(
///         &self, location: &LocationQuery
///     ) -> Result<Vec<Location>, Error<StandardRestError>> {
///         // Return a `RestRequest`. Do not return what is acually wanted by the function, it
///         // will be automatically generated for you.
///         RestRequest::<&LocationQuery, ()>::get("/locations").query(location)
///     }
/// }
/// ```
///
#[proc_macro_attribute]
pub fn rest(_: TokenStream, input: TokenStream) -> TokenStream {
    let mut ast = parse_macro_input!(input as ItemImpl);
    let self_ty = &ast.self_ty;
    let items = ast.items.iter().map(map_method).collect();
    ast.items = items;

    TokenStream::from(quote!(
            #[derive(Clone)]
            pub struct #self_ty {
                _rest: rrw::RestConfig
            }

            impl #self_ty {
                pub fn new(rest: rrw::RestConfig) -> Self {
                    Self {
                        _rest: rest
                    }
                }
            }

            #ast
    ))
}
